package com.uyung.koding.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.uyung.koding.MainActivity;
import com.uyung.koding.Model.DataModel;
import com.uyung.koding.R;

import java.util.List;


public class RecylerAdapter extends
        RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
    Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public RecylerAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(RecylerAdapter.MyHolder holder,
                                 final int position) {
        holder.namakampus.setText(mList.get(position).getnamakampus());
        holder.alamat.setText(mList.get(position).getalamat());
        holder.nhpkampus.setText(mList.get(position).getnhpkampus());
        holder.jumlahmhs.setText(mList.get(position).getjumlahmhs());
        holder.biaya.setText(mList.get(position).getbiaya());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View view) {
            Intent goInput = new Intent(ctx,MainActivity.class);
            try {
                goInput.putExtra("id", mList.get(position).getId());
                goInput.putExtra("namakampus", mList.get(position).getnamakampus());
                goInput.putExtra("alamat", mList.get(position).getalamat());
                goInput.putExtra("nhpkampus", mList.get(position).getnhpkampus());
                goInput.putExtra("jumlahmhs", mList.get(position).getjumlahmhs());
                goInput.putExtra("biaya", mList.get(position).getbiaya());
                ctx.startActivity(goInput);
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(ctx, "Error data " +e, Toast.LENGTH_SHORT).show();
            }
        }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView namakampus,alamat,nhpkampus,jumlahmhs,biaya;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            namakampus = (TextView) v.findViewById(R.id.tvnamakampus);
            alamat = (TextView) v.findViewById(R.id.tvalamat);
            nhpkampus = (TextView) v.findViewById(R.id.tvnhpkampus);
            jumlahmhs = (TextView) v.findViewById(R.id.tvjumlahmhs);
            biaya = (TextView) v.findViewById(R.id.tvbiaya);
        }
    }
}