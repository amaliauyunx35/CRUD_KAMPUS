package com.uyung.koding.Model;

import com.google.gson.annotations.SerializedName;

public class DataModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("namakampus")
    private String mnamakampus;
    @SerializedName("alamat")
    private String malamat;
    @SerializedName("nhpkampus")
    private String mnhpkampus;
    @SerializedName("jumlahmhs")
    private String mjumlahmhs;
    @SerializedName("biaya")
    private String mbiaya;

    public String getId() {
        return mId;
    }
    public void setId(String id) {
        mId = id;
    }
    public String getnamakampus() {
        return mnamakampus;
    }
    public void setnamakampus(String namakampus) {
        mnamakampus = namakampus;
    }
    public String getalamat() {
        return malamat;
    }
    public void setalamat(String alamat) {
        malamat = alamat;
    }
    public String getnhpkampus() {
        return mnhpkampus;
    }
    public void setnhpkampus(String nhpkampus) {
        mnhpkampus = nhpkampus;
    }
    public String getjumlahmhs() {
        return mjumlahmhs;
    }
    public void setjumlahmhs(String jumlahmhs) {
        mjumlahmhs = jumlahmhs;
    }
    public String getbiaya() {
        return mbiaya;
    }
    public void setbiaya(String biaya) {
        mbiaya = biaya;
    }

}
