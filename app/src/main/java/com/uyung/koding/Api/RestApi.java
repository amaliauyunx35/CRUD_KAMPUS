package com.uyung.koding.Api;

import com.uyung.koding.Model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RestApi {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendBiodata(@Field("namakampus") String namakampus,
                                    @Field("alamat") String alamat,
                                    @Field("nhpkampus") String nhpkampus,
                                    @Field("jumlahmhs") String jumlahmhs,
                                    @Field("biaya") String biaya);

    @GET("read.php")
    Call<ResponseModel> getBiodata();
    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData(@Field("id") String id,
                                   @Field("namakampus") String namakampus,
                                   @Field("alamat") String alamat,
                                   @Field("nhpkampus") String nhpkampus,
                                   @Field("jumlahmhs") String jumlahmhs,
                                   @Field("biaya") String biaya);

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> deleteData(@Field("id") String id);
}
